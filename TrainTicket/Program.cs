﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TrainTicket
{
    public class Program
    {
        public static string BerthType(int n)
        {
            string input = "SUB,LB,MB,UB,LB,MB,UB,SLB";
            List<string> serverNames = input.Split(',').ToList();
            return serverNames[n % 8];
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter number between 1 and 72");
            int n = Convert.ToInt32(Console.ReadLine());

            string result = BerthType(n);

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
