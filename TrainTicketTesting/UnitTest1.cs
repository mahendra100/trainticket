using TrainTicket;
using Xunit;

namespace TrainTicketTesting
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal("LB", Program.BerthType(1));
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal("SUB", Program.BerthType(72));
        }
    }
}
